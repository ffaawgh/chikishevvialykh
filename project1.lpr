program project1;
uses charset,crt;

var menuOption:integer;
    inputText : array [1..5] of string;
    RomanNumber: string;
    i: integer;
    
procedure OutputReverseNumber;
Var buffString: string;
    i,digit,result,number,lastDigit,firstDigit: integer;
begin
     firstDigit:= 0;
     for i := 1 to length(inputText[1]) do
         if (inputText[1][i] in ['0'..'9']) then
         begin
         if (firstDigit = 0) then
            firstDigit:=i
            else
            lastDigit:=i;
         end;
     for i := 1 to length(inputText[1]) do
         if (inputText[1][i] in ['0'..'9']) and (inputText[1][lastDigit+1] = ' ') and (inputText[1][firstDigit-1] = ' ') then
            buffString := buffString + inputText[1][i];
     if (length(buffString) = 0) then
         Writeln('There are no numbers in this string')
     else
         Writeln('The number in 1st sentence - ',buffString);
     val(buffString,number);
     result:=0;
        while number<>0 do begin
           digit:=number mod 2;
           number :=number div 2;
           result := digit+result*2;
     end;
        if (length(buffString) <> 0) then
     writeln('The binary reverse number is - ',result);
     readln;
end;

procedure OutputAlphabetQuote;
var
    allText,quoteStr: string;
    firstLetter:char;
    alphabetCheck:boolean;
    i, j, q, k, symbolCounter: integer;
begin
  alphabetCheck:=true;
  i:=1;
  symbolCounter:=0;
  allText:= inputText[1] + inputText[2] + inputText[3] + inputText[4] + inputText[5];
  quoteStr:=Copy(allText,Pos('"',allText)+1,Pos('"',Copy(allText,Pos('"',allText)+1,Length(allText)))-1);
  writeln(quoteStr,' - string to analys');
  while (quoteStr[i] <>'.') do
  begin
  while (quoteStr[i] <> ' ') do
  begin
    inc(symbolCounter);
    inc(i);
  end;
  firstLetter:=quotestr[1];
  delete(quoteStr,1,symbolCounter+1);
  if (quoteStr[1] < firstLetter) then
  begin
    alphabetCheck:=false;
    break;
  end;
  end;
  if (alphabetCheck = false) then
  writeln('This quote is not alplhabet row')
  else
  writeln('This quote is alplhabet row');
  i:=1;
  readln();
  readln();
end;
    
procedure HighlightMinimum;
var
    allText,w,mw: string;
    word,word1: array[1..100] of string;
    len: integer;
    ch:char;
    i, j, q, k, sameCounter: integer;

begin;
  allText:= inputText[1] + ' ' + inputText[2] + ' ' + inputText[3]+ ' ' + inputText[4] + ' ' + inputText[5];
  len := length(allText);
  i := 1;
  q := 0;
    while i <= len do
        if (lowercase(allText[i]) >= 'a') and (lowercase(allText[i]) <= 'z') then begin
            w := lowercase(allText[i]);
            i := i + 1;
            while (i <= len) and
            ((lowercase(allText[i]) >= 'a') and
            (lowercase(allText[i]) <= 'z')) do begin
                w := w + lowercase(allText[i]);
                i := i + 1;
            end;
            j := 1;
            while (j <= q)  do
                j := j + 1;
            if j > q then begin
                q := q + 1;
                word[q] := w;
            end;
        end
        else
            i := i + 1;

    mw:=word[1];
    for i:= 1 to q do
    begin
      if (length(word[i]) <= length(mw)) then
      mw:= word[i]
    end;
    i:=1;
    sameCounter:=0;
    writeln(allText);
    while (ch <> #8) do
    begin
      ch:=ReadKey;
      textcolor(green);
      writeln(mw);
      if (length(word[i]) = length(mw)) then
      begin
        mw:= word[i];
        inc(sameCounter);
      end;
      readln;
      textcolor(white);
      clrscr;
      writeln(allText);
      inc(i);
    end;
    writeln('The number of words with same length - ',sameCounter);
    readln;
end;
    
procedure OutputPalindromes;
   var s,sl,sk,sp,maxPolindrome: string;
    e,i,j,stringCounter: integer;
    PalindromeCheck: boolean;
begin
for stringCounter:= 1 to 5 do
begin
PalindromeCheck:=false;
s:= inputText[stringCounter];
writeln(s);
if (s[length(s)]<>' ') and (s[length(s)]<>'.') then
   maxPolindrome:='';
   s:=s+' ';
   sl:='';
for i:=1 to length(s) do
   if s[i]<>' ' then
     sl:=sl+s[i]
   else
      if (length(sl)>0) then
         begin
         sk:=''; sp:='';
         for e:=1 to length(sl) do
             sk:=sk+upcase(sl[e]);
         for e:=1 to length(sk) do
             sp:=sk[e]+sp;
         if (sp=sk) and ((length(sl)) > (length(maxPolindrome))) then
            begin
            maxPolindrome:=sl;
            PalindromeCheck:= true;
            end;
         sl:='';
         end;
         if (PalindromeCheck = false) or ( maxPolindrome = '"') then
         writeln('There are no palindromes')
         else
         writeln('Palindrome - ',maxPolindrome);
readln;
end;
readln;
end;

procedure RomanToNumber;

const
  TransTable: array[1..13] of record
    AN: word;
    RN: string[2];
  end = ((AN: 1000; RN:'M'),  (AN: 900; RN: 'CM'), (AN: 500; RN: 'D'),
         (AN: 400; RN: 'CD'), (AN: 100; RN: 'C'),  (AN: 90; RN: 'XC'),
         (AN: 50; RN: 'L'),   (AN: 40; RN: 'XL'),  (AN: 10; RN: 'X'),
         (AN: 9; RN: 'IX'),   (AN: 5; RN: 'V'),    (AN: 4; RN: 'IV'),
         (AN: 1; RN: 'I'));
var
arabianNumber: longint;
curNum: byte;
curSymb: string[2];
curVal: word;
begin
  writeln(inputText[5]);
  RomanNumber:=inputText[5];
  ArabianNumber := 0;
  curNum := 1;
repeat
curSymb := TransTable[CurNum].RN;
curVal := TransTable[CurNum].AN;
while copy(RomanNumber, 1, Length(CurSymb)) = CurSymb do
begin
Inc(ArabianNumber, CurVal);
Delete(RomanNumber, 1, Length(CurSymb));
end;
Inc(CurNum);
until RomanNumber = '';
WriteLn('Arabic equivalent for last sentence: ', ArabianNumber);
readln;
End;

Begin
  writeln('Enter text: ');
  for i:= 1 to 5 do
  readln(inputText[i]);
  repeat
  clrscr;
  writeln ('Select menu item:');
  writeln ('<1> Select the word containing the minimum number of characters');
  writeln ('<2> Print sentences, define the longest symmetric word in each');
  writeln ('<3> In the first sentence print the words according to a certain law');
  writeln ('<4> Convert Roman numbers to Arabic');
  writeln ('<5> Print quotes arranged alphabetically');
  writeln ('<6> Exit');
  write ('Number option:');
  readln(menuOption);
  clrscr;
	case (menuOption) of
	1: HighlightMinimum;
        2: OutputPalindromes;
	3: OutputReverseNumber;
	4: RomanToNumber;
	5: OutputAlphabetQuote;
	6: writeln('Good bye!');
	else
        writeln('Incorrect data!');
        end;
	until (menuOption = 6);
	writeln;
	readln;
End.
